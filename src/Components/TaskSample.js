import React from "react";
import './TaskSample.css';
import anupsir from "./Anupsir.png";
import arunsir from "./arun sir.jpeg";
import ranjansir from "./ranjan sir.jpeg";
import raashimam from "./raashi mam.jpeg";
import prabhnoormam from "./prabhnoor.jpeg";
import arvindsir from "./arvind.jpeg";
import hemantsir from "./hemantsir.jpeg";


function TaskSample() {
    return (
        <div>
            <h1>Meet the Team</h1>
            <div>
                <img className="ianup" src={anupsir} alt=''></img>
                <div className="anup">
                    <h2>Anup Raaj</h2>
                    <p className="anp">CEO & Managing Director</p>
                    <p>Anup being an IIT Bombay and Super 30 alumni has witnessed both best and
                    the least faciliated <br />frameworks of Indian education system and hence he knows
                    the best way to inculcate the learning<br /> methods for students His teaching.methods
                    are very unique that make the learning for students<br />faster and interesting.
                    </p>
                </div>
            </div>
            <div>
                <img className="iarun" src={arunsir} alt=''></img>
                <div className="arunsir">
                    <h2>Arun Kumar</h2>
                    <p className="arun">Co-Founder of 7classes</p>
                    <p>Arun has been involved in teaching from an early age with varied experience
                    and excellent track record.<br /> His expertise with Maths and Physics have made 
                    him a highly sought after teacher. As a very disciplined.<br />  individual he does
                    not hesitate to help weaker students.</p>
                </div>
            </div>
            <div>
                <img className="iranjan" src={ranjansir} alt=''></img>
                <div className="ranjansir">
                    <h2>Ranjan Voda</h2>
                    <p className="ranjan">Co-Founder and CPTO at 7classes</p>
                    <p>Ranjan is an alumni of NIT JSR. His unbeatable unique techniques to solve
                    mathematics problems makes<br />him students favourite. He has an expertise in
                    Olympiads as well. His out of the box teaching <br />techniques have proven very 
                    successful for weak students and they ought to perform extremely well.</p>
                </div>
            </div>
            <div>
                <img className="iarvind" src={arvindsir} alt=''></img>
                <div className="arvindsir">
                    <h2>Tr. Arvind</h2>
                    <p className="arvind">Co-Founder and CMO</p>
                    <p>Arvind being an alumni of IIT Bombay, is the best faculty for Chemistry for 
                    IIT JEE not from now<br />but from he is teaching chemistry from an early age. His 
                    unique teaching methods and expertise<br />in providing tricks and nicks for the 
                    exam makes him absoulte favourite among his students.</p>
                </div>
            </div>
            <div>
                <img className="ihemant" src={hemantsir} alt=''></img>
                <div className="hemantsir">
                    <h2>Hemant Singh Rajput</h2>
                    <p className="hemant">Operation Head</p>
                    <p>He is Co-founder and operation head at instapreps & 7classes. He completed his 
                        graduation in computer science from<br /> Ranchi Univercity (session 2011-2014)
                    </p>
                </div>
            </div>
            <div>
                <img className="iraashi" src={raashimam} alt=''></img>
                <div className="raashimam">
                    <h2>Raashi Sharma</h2>
                    <p className="raashi">Software Engineer</p>
                    <p>As a highly motivated and skill developer. She is possionate about coding and 
                    learning how to code efficiently.<br /> With excelent problem solving skills. She has 
                    succesfully solved over 350+ DSA quastions an leetcode using java.</p>
                </div>
            </div>
            <div>
                <img className="iprabhnoor" src={prabhnoormam} alt=''></img>
                <div className="prabhnoormam">
                    <h2>Prabhnoor Kaur</h2>
                    <p className="prabhnoor">EA to the CEO</p>
                    <p>She is an EA TO CEO of 7classes. She belongs to jalandhar Punjab. She has 
                    <br />studied at keshav mahavidhyalaya. She completed her internship in MS Excel.</p>
                </div>
            </div>
            <footer>Thank You</footer>
        </div>
    
    )
}

export default TaskSample;