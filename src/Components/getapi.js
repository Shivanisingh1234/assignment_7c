import react, { useEffect, useState } from 'react'

function App() {
    const [data, setData] =useState([])
    useEffect(() =>{
      fetch("https://staging.instapreps.com/api/board").then((result) =>{
      result.json().then((resp) =>{
        console.warn("result",resp)
        setData (resp)
      })
    })
  }, [])
    return (
      <div className="App">
        <h1>api call</h1>
        <table border={1}>
          <tr>  
            <td>id</td>
            <td>board</td>
            <td>created_at</td>
            <td>updated_at</td>
            <td>priority</td>
            <td>publish</td>
          </tr>
          {
            data.map((iteam) =>
            <tr>
              <td>{iteam.id}</td>
              <td>{iteam.board}</td>
              <td>{iteam.created_at}</td>
              <td>{iteam.updated_at}</td>
              <td>{iteam.priority}</td>
              <td>{iteam.publish}</td>
            </tr>
            )
          }
        </table>
    </div>
  );
}


