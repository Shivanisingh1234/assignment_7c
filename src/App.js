import logo from './logo.svg';
import './App.css';
import TaskSample from './Components/TaskSample';

function App() {
    return (
      <div className="App">
        <TaskSample />
      </div>
    );
  }
export default App;
